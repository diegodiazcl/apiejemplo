'use strict';

module.exports = function(Alerta) {
    Alerta.defineProperty('userId', { type: require('mongodb').ObjectId });
    Alerta.defineProperty('tiposAlertaId', { type: require('mongodb').ObjectId });
    Alerta.defineProperty('gruposUserId', { type: require('mongodb').ObjectId });
};
